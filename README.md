
# Software deployment - Ansible : Examen


## Exercice 1 (4 pts)

Ecrire un inventaire qui répond aux critères suivants:
6 hosts :
* 8vh63p.mon-super-domain.bzh
	* user: user1
	* port: 54000
* iimzhr.mon-super-domain.bzh
	* user: user1
	* port: 54000
* 6b7vav.mon-super-domain.bzh
	* user: user2
* ruyf2s.mon-super-domain.bzh
	* user: user2
* 77yg05.mon-super-domain.bzh
	* user: user2
* 156.65.81.2

Découper ces 6 hosts en 3 groupes.
  * ui
 * back
 * bdd

Ajouter localhost comme host non groupé

## Exercice 2 (9 pts)

Cet exercice consiste en déployer une application angular déjà buildé dans un NGINX, préconfiguré.

L’application va chercher dans un fichier config.json 3 variables qui seront à valoriser durant le déploiement.

Le fichier config.json à ce format:
``` json
{
	“nom”: “Gestin”,
	“prenom”: “Gweltaz”,
	“promo”: “ISTIC Master 2 GL”
}
```

fichier fournis: archive contenant l’application (app.zip)
le fichier de configuration nginx (default)

**Partie 1 :** Template
Créer un template pour pouvoir générer le fichier config.json avec vos information
Utiliser ansible en ligne de commande pour valider que le template fonctionne bien.

**Partie 2 :** Inventaire Vous allez déployer ce NGINX en local sur localhost.
Ecrire l’inventaire qui vous le permettra.

**Partie 3 :** Installer NGINX
Écrivez un playbook pour installer NGINX sur localhost.

**Partie 4 :** Déployer l’application
Ajouter des tâches pour déployer l’application dans le NGINX nouvellement installer.
Note : le fichier config.json est à mettre dans le dossier assets de l’application déployé

**Partie 5 :** Stop et uninstall Ajouté la possibilité de stopper et désinstaller Note: utiliser des tags

**Partie 6 :** Config.json
Ajouter un tableau de string dans le config.json
Le nom de la variable à ajouté est hobbies

### En bonus partir d’ici :

**Bonus 1 :** Vault

Ajouter une variable secret dans le fichier config.json.
Cette nouvelle variable devra être encrypter.

* `ansible-playbook -i hosts.yml -t vars -t install -t start playbook.yml --vault-password-file password_file`

**Bonus 2 :**  Fichier de variables
Utiliser un fichier de variable pour les variables

**Bonus 3 :**  multi environnement
Ajouter des varaibles pour d'autres environnements, fournir les commandes pour  lancer les différents environnement (prod, dev, preprod)

* `ansible-playbook -i hosts.yml -t dev -t install -t start playbook.yml --vault-password-file password_file`
* `ansible-playbook -i hosts.yml -t preprod -t install -t start playbook.yml --vault-password-file password_file`
* `ansible-playbook -i hosts.yml -t prod -t install -t start playbook.yml --vault-password-file password_file`
